# 新版PKE文档已移至：[gitee.com/hustos/pke-doc](https://gitee.com/hustos/pke-doc)

# 代码已移至：[gitee.com/hustos/riscv-pke](https://gitee.com/hustos/riscv-pke)


## 基于RISC-V代理内核的操作系统课程实验与课程设计(before 2021)

[前言](preliminary.md)

[第一章. RISC-V体系结构](chapter1.md)  
- [1.1 RISC-V发展历史](chapter1.md#history)  
- [1.2 RISC-V汇编语言](chapter1.md#assembly)  
- [1.3 机器的特权状态](chapter1.md#machinestates)  
- [1.4 中断和中断处理](chapter1.md#traps)  
- [1.5 页式虚存管理](chapter1.md#paging)  
- [1.6 什么是代理内核](chapter1.md#proxykernel)  
- [1.7 相关工具软件](chapter1.md#toolsoftware)  

[第二章. （实验1）非法指令的截获](chapter2.md)

[第三章. （实验2）系统调用的实现](chapter3.md)

[第四章. （实验3）物理内存管理](chapter4.md)

[第五章. （实验4）缺页异常的处理](chapter5.md)

[第六章. （实验5）进程的封装](chapter6.md)

[课程设计](课程设计.md)

